<?php

declare(strict_types=1);

namespace Skript\Utils\_Array;

require_once __DIR__.'/../functions.php';

/**
 * Alternative OO interface
 */
class Dict extends \ArrayObject
{
    public function pick(array $keys, array $defaults=[]): self
    {
        return new self(pick($this->getArrayCopy(), $keys, $defaults));
    }
    
    public function pickWithDefaults(array $keyedDefaults): self
    {
        return new self(
            pickWithDefaults($this->getArrayCopy(), $keyedDefaults)
        );
    }
    
    public function keys(): array
    {
        return array_keys($this->getArrayCopy());
    }
    
    public function values(): array
    {
        return array_values($this->getArrayCopy());
    }
    
    public function prepend(...$elements): self
    {
        $newArray = $this->getArrayCopy();
        array_unshift($newArray, ...$elements);
        $this->exchangeArray($newArray);
        return $this;
    }
    
    /**
     * Alias for getArrayCopy
     */
    public function toArray(): array
    {
        return $this->getArrayCopy();
    }
}